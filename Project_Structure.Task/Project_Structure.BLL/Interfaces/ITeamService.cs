﻿using Project_Structure.BLL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        public Task<IEnumerable<TeamDTO>> GetAllTeams();
        public Task<TeamDTO> GetTeamById(int id);
        public Task<TeamDTO> CreateTeam(TeamDTO team);
        public Task<TeamDTO> UpdateTeam(TeamDTO team);
        public Task DeleteTeam(int idTask);
    }
}
