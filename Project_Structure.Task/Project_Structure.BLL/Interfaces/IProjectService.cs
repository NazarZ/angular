﻿using Project_Structure.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectService : IDisposable
    {
        public Task<IEnumerable<ProjectDTO>> GetAllProjects();
        public Task<ProjectDTO> GetProjectById(int id);
        public Task<ProjectDTO> CreateProject(ProjectDTO project);
        public Task<ProjectDTO> UpdateProject(ProjectDTO project);
        public Task DeleteProject(int idProject);
    }
}
