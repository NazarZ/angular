﻿using AutoMapper;
using Binary.Linq.BL.Models.Enum;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Models;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class QueryService : BaseService, IQueryService
    {
        private List<ProjectModel> _projectsModel;
        public QueryService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
            LoadData();
        }
        private void LoadData()
        {
            _projectsModel = (from project in _context.Projects.GetAll().Result
                              join author in _context.Users.GetAll().Result on project.AuthorId equals author.Id
                              join team in _context.Teams.GetAll().Result on project.TeamId equals team.Id
                              select new ProjectModel
                              {
                                  Project = project,
                                  Tasks = (from task in _context.Tasks.GetAll().Result
                                           join performer in _context.Users.GetAll().Result on task.PerformerId equals performer.Id
                                           where task.ProjectId == project.Id
                                           select new TaskModel
                                           {
                                               Task = task,
                                               Performer = performer
                                           }).ToList(),
                                  Author = author,
                                  Team = team
                              }).ToList();
        }
        public Task<Dictionary<ProjectModel, int>> GetCountTasksUserInProjects(int userId)
        {
            return Task.Run<Dictionary<ProjectModel, int>>(() =>
           {
               return _projectsModel.Select(p => new
               {
                   project = p,
                   count = p.Tasks.Count(x => x.Performer.Id == userId)
               }).ToDictionary(k => k.project, v => v.count);
           });

        }

        public Task<List<FinishedTaskModel>> GetFinishedTaskByUserId(int userId)
        {
            return Task.Run(() =>
                {
                    return _projectsModel.SelectMany(t => t.Tasks)
                    .Where(x => x.Performer.Id == userId && x.Task.FinishedAt.GetValueOrDefault().Year == 2021)
                    .Select(x => new FinishedTaskModel { Id = x.Task.Id, Name = x.Task.Name })
                    .ToList();
                });
            
        }
        public async Task<List<TeamUsers>> GetTeams()
        {
            var teams = await _context.Teams.GetAll();
            var users = await _context.Users.GetAll();

            return (from team in teams
                        group team by team into t
                        select new TeamUsers
                        {
                            Id = t.Key.Id,
                            TeamName = t.Key.Name,
                            Users = users.Where(u => u.BirthDay.Year < 2011 && u.TeamId == t.Key.Id)
                                         .OrderByDescending(x => x.RegisteredAt).ToList()
                        }).ToList();
            
        }

        public Task<Task6Model> GetTask6(int userId)
        {
            return Task.Run<Task6Model>(() =>
            {
                return (from user in _context.Users.GetAll().Result
                        where user.Id == userId
                        let project = _projectsModel.Where(x => x.Author.Id == user.Id)
                                                   .OrderByDescending(x => x.Project.CreatedAt)
                                                   .FirstOrDefault() ?? new ProjectModel()
                        select new Task6Model
                        {
                            User = user,
                            LastProjectUser = project.Project ?? null,
                            TotalTaskInLastProject = project.Tasks?.Count ?? 0,
                            TotalCountUnfinishedTask = _projectsModel.SelectMany(x => x.Tasks)
                                                                    .Where(y => y.Task.State == TaskState.Third && y.Task.PerformerId == user.Id)
                                                                    .Count(),
                            LongestUserTask = _projectsModel.SelectMany(x => x.Tasks)
                                                           .Where(x => x.Task.PerformerId == userId && x.Task.FinishedAt is not null)
                                                           .OrderByDescending(x => x.Task.FinishedAt - x.Task.CreatedAt)
                                                           .Select(x => x.Task)
                                                           .FirstOrDefault()

                        }).FirstOrDefault();
            });

        }

        public async Task<List<TaskModel>> GetTasksByUserId(int userId)
        {
            return _projectsModel.SelectMany(t => t.Tasks)
                                 .Where(x => x.Performer.Id == userId && x.Task.Name.Length < 45)
                                 .Select(x => new TaskModel { Task = x.Task, Performer = x.Performer })
                                 .ToList();
        }

        public async Task<List<UserWithTaskModel>> GetUserOrderByNameWithTask()
        {
            return (from user in _context.Users.GetAll().Result
                    orderby user.FirstName
                    select new UserWithTaskModel
                    {
                        User = user,
                        Tasks = _context.Tasks.GetAll().Result.Where(t => t.PerformerId == user.Id).OrderByDescending(x => x.Name.Length).ToList()
                    }).ToList();
        }
    }
}
