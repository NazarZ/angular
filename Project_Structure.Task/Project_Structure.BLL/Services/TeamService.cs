﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public async Task<TeamDTO> CreateTeam(TeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            await _context.Teams.Create(teamEntity);
            await _context.SaveAsync();
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public async System.Threading.Tasks.Task DeleteTeam(int idTask)
        {
            await _context.Teams.Delete(idTask);
            await _context.SaveAsync();
        }

        public async Task<IEnumerable<TeamDTO>> GetAllTeams()
        {

            return _mapper.Map<IEnumerable<TeamDTO>>(await _context.Teams.GetAll());
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            return _mapper.Map<TeamDTO>(await _context.Teams.Get(id));
        }

        public async Task<TeamDTO>UpdateTeam(TeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            await _context.Teams.Update(teamEntity);
            await _context.SaveAsync();
            return _mapper.Map<TeamDTO>(teamEntity);
        }
    }
}
