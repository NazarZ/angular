﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class TaskRepository : BaseRepository, IRepository<Task>
    {
        public TaskRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public System.Threading.Tasks.Task Create(Task item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                item.Id = 0;
                _db.Tasks.Add(item);
            });
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var toRemove = _db.Tasks.SingleOrDefault(x => x.Id == id);
                if (toRemove is not null)
                {
                    _db.Tasks.Remove(toRemove);
                }
                else
                {
                    throw new ArgumentException("Task does not exist", "404");
                }
            });
        }

        public System.Threading.Tasks.Task<IEnumerable<Task>> Find(Func<Task, bool> predicate)
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<Task>>(() =>
            {
                return _db.Tasks.AsNoTracking().Where(predicate).ToList();
            });
        }

        public System.Threading.Tasks.Task<Task> Get(int id)
        {
            return System.Threading.Tasks.Task.Run<Task>(() =>
            {
                return _db.Tasks.AsNoTracking().SingleOrDefault(x => x.Id == id);
            });
        }

        public System.Threading.Tasks.Task<IEnumerable<Task>> GetAll()
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<Task>>(() =>
            {
                return _db.Tasks.AsNoTracking().ToList();
            });
        }

        public System.Threading.Tasks.Task Update(Task item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var trackedTask = _db.Tasks.Find(item.Id);
                _db.Entry(trackedTask).CurrentValues.SetValues(item);
            });
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
