﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class Team : BaseEntity
    {
        [MaxLength(200)]
        public string? Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
    }
}
