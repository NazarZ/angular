﻿using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Project_Structure.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        IUserService _userService;
        public UsersController(IUserService service)
        {
            _userService = service;
        }
        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            Response.ContentType = "application/json";
            return Ok(await _userService.GetAllUsers());
        }

        // GET: api/Users/id
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                var result = await _userService.GetUserById(Convert.ToInt32(id));
                return result is not null ? Ok(result) : NotFound();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
        }
        // POST: api/Users
        //create
        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserDTO user)
        {
            try
            {
                if (user is not null)
                {
                    var created = await _userService.CreateUser(user);
                    Response.ContentType = "application/json";
                    return Created("", created);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // PUT: api/Users
        //Update
        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UserDTO user)
        {
            try
            {
                if (user is not null)
                {
                    await _userService.UpdateUser(user);
                    Response.ContentType = "application/json";
                    return Created("", user);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }



        // DELETE: api/Users/id
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                await _userService.DeleteUser(Convert.ToInt32(id));
                return NoContent();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
