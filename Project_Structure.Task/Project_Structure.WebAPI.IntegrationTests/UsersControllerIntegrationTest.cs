﻿using Newtonsoft.Json;
using Project_Structure.BLL.DTOs;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Project_Structure.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _customWebApplicationFactory;
        private readonly HttpClient _client;
        private string _requestUri;
        public UsersControllerIntegrationTest(CustomWebApplicationFactory customWebApplicationFactory)
        {
            _customWebApplicationFactory = customWebApplicationFactory;
            _client = _customWebApplicationFactory.CreateClient();
            _requestUri = _client.BaseAddress.AbsoluteUri + "api/Users";
        }

        [Fact]
        public async Task GetUsersResource_HttpResponse_ShouldReturn200OK()
        {
            // Arrange

            // Act
            var sut = await _client.GetAsync(_requestUri);

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.OK, responseCode);
        }

        [Theory]
        [InlineData("{\"id\": 0,\"teamId\": null,\"firstName\": \"Name\",\"lastName\": \"Last Name\",\"email\": \"my.email@gamail.com\",\"registeredAt\": \"2021-07-05T13:14:41.082Z\",\"birthDay\": \"2021-07-05T13:14:41.082Z\"}")]
        public async Task CreateUser_Then_HttpResponse_ShouldReturn201Created(string jsonString)
        {
            // Arrange

            // Act
            var httpResponse = await _client.PostAsync(_requestUri, new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);


            // Assert 
            var responseCode = httpResponse.StatusCode;
            Assert.Equal(HttpStatusCode.Created, responseCode);
            Assert.NotNull(createdUser);

        }

        [Fact]
        public async Task RemoveUser_Then_HttpResponse_ShouldReturn204NoContent()
        {
            // Arrange
            var user = new UserDTO() { FirstName = "New UserName", LastName = "New User Last Name", BirthDay = DateTime.Now.AddYears(-20), RegisteredAt = DateTime.Now };
            var cratedUser = await CreateUser(user);
            // Act
            var httpResponse = await _client.DeleteAsync($"{_requestUri}/{cratedUser.Id}");
            var getResponse = await _client.GetAsync($"{_requestUri}/{cratedUser.Id}");

            // Assert 
            var responseCode = httpResponse.StatusCode;
            Assert.Equal(HttpStatusCode.NoContent, responseCode);
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }
        [Fact]
        public async Task RemoveUser_When_UserDoesNotExist_Then_HttpResponse_ShouldReturn404NotFound()
        {
            // Arrange
            var user = new UserDTO() { FirstName = "New UserName", LastName = "New User Last Name", BirthDay = DateTime.Now.AddYears(-20), RegisteredAt = DateTime.Now };
            var cratedUser = await CreateUser(user);
            // Act
            var httpResponse = await _client.DeleteAsync($"{_requestUri}/{cratedUser.Id}");
            httpResponse = await _client.DeleteAsync($"{_requestUri}/{cratedUser.Id}");

            // Assert 
            var responseCode = httpResponse.StatusCode;
            Assert.Equal(HttpStatusCode.NotFound, responseCode);
        }

        internal async Task<UserDTO> CreateUser(UserDTO user)
        {
            var json = JsonConvert.SerializeObject(user);
            var httpResponse = await _client.PostAsync(_requestUri, new StringContent(json, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);
            return createdUser;
        }




    }
}
