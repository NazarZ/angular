﻿using Binary.Linq.BL.Models.Enum;
using Newtonsoft.Json;
using Project_Structure.BLL.DTOs;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Project_Structure.WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _customWebApplicationFactory;
        private readonly HttpClient _client;
        private string _requestUri;

        public TasksControllerIntegrationTest(CustomWebApplicationFactory customWebApplicationFactory)
        {
            _customWebApplicationFactory = customWebApplicationFactory;
            _client = _customWebApplicationFactory.CreateClient();
            _requestUri = _client.BaseAddress.AbsoluteUri + "api/Tasks";
        }

        [Fact]
        public async Task GetTaskResource_HttpResponse_ShouldReturn200OK()
        {
            // Arrange

            // Act
            var sut = await _client.GetAsync(_requestUri);

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.OK, responseCode);
        }
        [Fact]
        public async Task RemoveTask_Then_HttpResponse_ShouldReturn204NoContent()
        {
            // Arrange
            var cratedTask = await CreateTask(new ProjectDTO
            {
                Name = "Project Name",
                Description = "Project description",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(2),
            },
            new UserDTO()
            {
                FirstName = "Name",
                LastName = "Last Name",
                BirthDay = DateTime.Now
            },
            new TeamDTO()
            {
                Name = "New Team",
                CreatedAt = DateTime.Now
            },
            "New Task", "Task Description", TaskState.First);

            var taskJson = JsonConvert.SerializeObject(cratedTask);

            // Act
            var httpResponse = await _client.DeleteAsync($"{_requestUri}/{cratedTask.Id}");

            // Assert 
            var responseCode = httpResponse.StatusCode;
            Assert.Equal(HttpStatusCode.NoContent, responseCode);
        }
        [Fact]
        public async Task RemoveTask_UserDoesNotExist_Then_HttpResponse_ShouldReturn404NotFound()
        {
            // Arrange
            var cratedTask = await CreateTask(new ProjectDTO
            {
                Name = "Project Name",
                Description = "Project description",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(2),
            },
            new UserDTO()
            {
                FirstName = "Name",
                LastName = "Last Name",
                BirthDay = DateTime.Now
            },
            new TeamDTO()
            {
                Name = "New Team",
                CreatedAt = DateTime.Now
            },
            "New Task", "Task Description", TaskState.First);

            var taskJson = JsonConvert.SerializeObject(cratedTask);

            // Act
            var httpResponse = await _client.DeleteAsync($"{_requestUri}/{cratedTask.Id}");
            httpResponse = await _client.DeleteAsync($"{_requestUri}/{cratedTask.Id}");

            // Assert 
            var responseCode = httpResponse.StatusCode;
            Assert.Equal(HttpStatusCode.NotFound, responseCode);
        }




        private async Task<TaskDTO> CreateTask(ProjectDTO project, UserDTO user, TeamDTO team, string name, string description, TaskState state)
        {
            var createdTeam = await CreateTeam(team);
            user.TeamId = createdTeam.Id;
            var createdUser = await CreateUser(user);
            project.TeamId = createdTeam.Id;
            project.AuthorId = user.Id;
            var createdProject = await CreateProject(project);

            var taskJson = JsonConvert.SerializeObject(new TaskDTO()
            {
                Name = name,
                Description = description,
                State = state,
                ProjectId = createdProject.Id,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddMonths(-2),
                FinishedAt = DateTime.Now
            });

            var httpResponse = await _client.PostAsync(_requestUri, new StringContent(taskJson, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);
            return createdTask;
        }

        internal async Task<UserDTO> CreateUser(UserDTO user)
        {
            var json = JsonConvert.SerializeObject(user);
            var httpResponse = await _client.PostAsync(_client.BaseAddress.AbsoluteUri + "api/Users", new StringContent(json, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);
            return createdUser;
        }
        internal async Task<TeamDTO> CreateTeam(TeamDTO team)
        {
            var json = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync(_client.BaseAddress.AbsoluteUri + "api/Teams", new StringContent(json, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);
            return createdTeam;
        }
        internal async Task<ProjectDTO> CreateProject(ProjectDTO project)
        {
            var json = JsonConvert.SerializeObject(project);
            var httpResponse = await _client.PostAsync(_client.BaseAddress.AbsoluteUri + "api/Teams", new StringContent(json, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);
            return createdProject;
        }


    }
}
