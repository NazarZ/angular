﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Project_Structure.WebAPI.IntegrationTests
{
    public class QueriesControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _customWebApplicationFactory;
        private readonly HttpClient _client;
        private string _requestUri;

        public QueriesControllerIntegrationTest(CustomWebApplicationFactory customWebApplicationFactory)
        {
            _customWebApplicationFactory = customWebApplicationFactory;
            _client = _customWebApplicationFactory.CreateClient();
            _requestUri = _client.BaseAddress.AbsoluteUri + "api/Queries";
        }
        [Fact]
        public async Task GetFinishedTaskByUserIdResource_HttpResponse_ShouldReturn200OK()
        {
            // Arrange

            // Act
            var sut = await _client.GetAsync(_requestUri + $"/GetFinishedTaskByUserId/{25}");

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.OK, responseCode);
        }
        [Fact]
        public async Task GetFinishedTaskByUserIdResource_With_BadID_HttpResponse_ShouldReturn400BadRequest()
        {
            // Arrange
            var id = "id";
            // Act
            var sut = await _client.GetAsync(_requestUri + $"/GetFinishedTaskByUserId/{id}");

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.BadRequest, responseCode);
        }


    }
}
