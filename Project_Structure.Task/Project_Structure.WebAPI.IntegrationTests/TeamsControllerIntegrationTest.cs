﻿using Newtonsoft.Json;
using Project_Structure.BLL.DTOs;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Project_Structure.WebAPI.IntegrationTests
{
    public class TeamsControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _customWebApplicationFactory;
        private readonly HttpClient _client;
        private string _requestUri;

        public TeamsControllerIntegrationTest(CustomWebApplicationFactory customWebApplicationFactory)
        {
            _customWebApplicationFactory = customWebApplicationFactory;
            _client = _customWebApplicationFactory.CreateClient();
            _requestUri = _client.BaseAddress.AbsoluteUri + "api/Teams";
        }
        [Fact]
        public async Task GetTeamsResource_HttpResponse_ShouldReturn200OK()
        {
            // Arrange

            // Act
            var sut = await _client.GetAsync(_requestUri);

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.OK, responseCode);
        }

        [Theory]
        [InlineData("{\"id\": 0,\"name\": \"New Team\",\"createdAt\": \"2021-07-05T15:00:02.361Z\"}")]
        public async Task CreateTeam_Then_HttpResponse_ShouldReturn201Created(string jsonString)
        {
            // Arrange

            // Act
            var httpResponse = await _client.PostAsync(_requestUri, new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            // Assert 
            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.NotNull(createdTeam);

        }
        [Theory]
        [InlineData("{\"id\": 0,\"name\": \"New Team\",\"createdAt\": \"0000-00-00T00:00:00.000Z\"}")]
        [InlineData("{\"id\": 0,\"name\": \"New ")]
        public async Task CreateTeam_When_IncorrectData_Then_HttpResponse_ShouldReturn400BadRequest(string jsonString)
        {
            // Arrange

            // Act
            var httpResponse = await _client.PostAsync(_requestUri, new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            // Assert 
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
            Assert.NotNull(createdTeam);

        }
    }
}
