import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { UsersService } from '../users.service';
import { User } from '../user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  dataSaved = false;
  userForm: any;
  allUsers?: Observable<User[]>;
  userIdUpdate: undefined | string = undefined;
  massage: string | null = null;
  title = 'Users';
  constructor(
    private formbulider: FormBuilder,
    private userService: UsersService
  ) {}

  ngOnInit() {
    this.userForm = this.formbulider.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      teamId: [''],
      birthDay: ['', [Validators.required]],
    });
    this.loadAllUsers();
  }
  loadAllUsers() {
    this.allUsers = this.userService.getAllUser();
  }
  onFormSubmit() {
    this.dataSaved = false;
    const user = this.userForm.value;
    this.CreateUser(user);
    this.userForm.reset();
  }
  loadUserToEdit(userId: string | undefined) {
    this.userService.getUserById(userId).subscribe((user) => {
      this.massage = null;
      this.dataSaved = false;
      this.userIdUpdate = user.id!;
      this.userForm.controls['firstName'].setValue(user.firstName);
      this.userForm.controls['lastName'].setValue(user.lastName);
      this.userForm.controls['email'].setValue(user.email);
      this.userForm.controls['teamId'].setValue(user.teamId);
      this.userForm.controls['birthDay'].setValue(user.birthDay);
    });
  }
  CreateUser(user: User) {
    if (this.userIdUpdate == undefined) {
      this.userService.createUser(user).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record saved Successfully';
        this.loadAllUsers();
        this.userIdUpdate = undefined;
        this.userForm.reset();
      });
    } else {
      user.id = this.userIdUpdate;
      this.userService.updateUser(user).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Updated Successfully';
        this.loadAllUsers();
        this.userIdUpdate = undefined;
        this.userForm.reset();
      });
    }
  }
  deleteUser(userId: string) {
    if (confirm('Are you sure you want to delete this ?')) {
      console.log('delete: ' + userId);
      this.userService.deleteUserById(userId).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Deleted Succefully';
        this.loadAllUsers();
        this.userIdUpdate = undefined;
        this.userForm.reset();
      });
    }
  }
  resetForm() {
    this.userForm.reset();
    this.massage = null;
    this.dataSaved = false;
  }
}
