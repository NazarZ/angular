export class User {
  id: string;
  teamId: string;
  firstName: string;
  lastName: string;
  email: string;
  registeredAt: Date;
  birthDay: Date;
}
