import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  url = 'https://localhost:44362';
  constructor(private http: HttpClient) {}
  getAllUser(): Observable<User[]> {
    return this.http.get<User[]>(this.url + '/api/Users');
  }
  getUserById(UserId: string | undefined): Observable<User> {
    return this.http.get<User>(this.url + '/api/Users/' + UserId);
  }
  createUser(User: User): Observable<User> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    User.registeredAt = new Date();
    return this.http.post<User>(this.url + '/api/Users/', User, httpOptions);
  }
  updateUser(User: User): Observable<User> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    console.log(User);
    return this.http.put<User>(this.url + '/api/Users/', User, httpOptions);
  }
  deleteUserById(Userid: string): Observable<number> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.http.delete<number>(
      this.url + '/api/Users/' + Userid,
      httpOptions
    );
  }
}
