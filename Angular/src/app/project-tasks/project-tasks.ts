export class ProjectTasks {
  id: string;
  projectId: string;
  performerId: string;
  name: string;
  description: string;
  state: number;
  createdAt: Date;
  finishedAt: Date;
}
