import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProjectTasks } from './project-tasks';

@Injectable({
  providedIn: 'root',
})
export class ProjectTasksService {
  url = 'https://localhost:44362';
  constructor(private http: HttpClient) {}
  getAllProjectTasks(): Observable<ProjectTasks[]> {
    return this.http.get<ProjectTasks[]>(this.url + '/api/Tasks');
  }
  getProjectTasksById(
    ProjectTasksId: string | undefined
  ): Observable<ProjectTasks> {
    return this.http.get<ProjectTasks>(
      this.url + '/api/Tasks/' + ProjectTasksId
    );
  }
  createProjectTasks(ProjectTasks: ProjectTasks): Observable<ProjectTasks> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    ProjectTasks.createdAt = new Date();
    return this.http.post<ProjectTasks>(
      this.url + '/api/Tasks/',
      ProjectTasks,
      httpOptions
    );
  }
  updateProjectTasks(ProjectTasks: ProjectTasks): Observable<ProjectTasks> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    console.log(ProjectTasks);
    return this.http.put<ProjectTasks>(
      this.url + '/api/Tasks/',
      ProjectTasks,
      httpOptions
    );
  }
  deleteProjectTasksById(ProjectTasksid: string): Observable<number> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.http.delete<number>(
      this.url + '/api/Tasks/' + ProjectTasksid,
      httpOptions
    );
  }
}
