import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ProjectTasksService } from '../project-tasks.service';
import { ProjectTasks } from '../project-tasks';

@Component({
  selector: 'app-project-tasks',
  templateUrl: './project-tasks.component.html',
  styleUrls: ['./project-tasks.component.css'],
})
export class ProjectTasksComponent implements OnInit {
  dataSaved = false;
  taskForm: any;
  allProjectTasks?: Observable<ProjectTasks[]>;
  taskIdUpdate: undefined | string = undefined;
  massage: string | null = null;
  title = 'ProjectTaskss';
  today = new Date();
  constructor(
    private formbulider: FormBuilder,
    private taskService: ProjectTasksService
  ) {}

  ngOnInit() {
    this.taskForm = this.formbulider.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      projectId: ['', [Validators.required]],
      performerId: ['', [Validators.required]],
      state: ['', [Validators.required]],
      finishedAt: [''],
    });
    this.loadAllProjectTasks();
  }
  loadAllProjectTasks() {
    this.allProjectTasks = this.taskService.getAllProjectTasks();
  }
  onFormSubmit() {
    this.dataSaved = false;
    const task = this.taskForm.value;
    this.CreateProjectTasks(task);
    this.taskForm.reset();
  }
  loadProjectTasksToEdit(taskId: string | undefined) {
    this.taskService.getProjectTasksById(taskId).subscribe((task) => {
      this.massage = null;
      this.dataSaved = false;
      this.taskIdUpdate = task.id!;
      this.taskForm.controls['name'].setValue(task.name);
      this.taskForm.controls['description'].setValue(task.description);
      this.taskForm.controls['projectId'].setValue(task.projectId);
      this.taskForm.controls['performerId'].setValue(task.performerId);
      this.taskForm.controls['state'].setValue(task.state);
      this.taskForm.controls['finishedAt'].setValue(task.finishedAt);
    });
  }
  CreateProjectTasks(task: ProjectTasks) {
    if (this.taskIdUpdate == undefined) {
      this.taskService.createProjectTasks(task).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record saved Successfully';
        this.loadAllProjectTasks();
        this.taskIdUpdate = undefined;
        this.taskForm.reset();
      });
    } else {
      task.id = this.taskIdUpdate;
      this.taskService.updateProjectTasks(task).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Updated Successfully';
        this.loadAllProjectTasks();
        this.taskIdUpdate = undefined;
        this.taskForm.reset();
      });
    }
  }
  deleteProjectTasks(taskId: string) {
    if (confirm('Are you sure you want to delete this ?')) {
      console.log('delete: ' + taskId);
      this.taskService.deleteProjectTasksById(taskId).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Deleted Succefully';
        this.loadAllProjectTasks();
        this.taskIdUpdate = undefined;
        this.taskForm.reset();
      });
    }
  }
  resetForm() {
    this.taskForm.reset();
    this.massage = null;
    this.dataSaved = false;
  }
  isTaskComplete(dateFirst: Date, dateSecond: Date): boolean {
    return new Date(dateFirst).getTime() < new Date(dateSecond).getTime();
  }
}
