import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ProjectsService } from '../projects.service';
import { Project } from '../project';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
})
export class ProjectsComponent implements OnInit {
  dataSaved = false;
  projectForm: any;
  allProjects?: Observable<Project[]>;
  projectIdUpdate: undefined | string = undefined;
  massage: string | null = null;
  title = 'Projects';
  constructor(
    private formbulider: FormBuilder,
    private projectService: ProjectsService
  ) {}

  ngOnInit() {
    this.projectForm = this.formbulider.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      deadline: ['', [Validators.required]],
      teamId: ['', [Validators.required]],
      authorId: ['', [Validators.required]],
    });
    this.loadAllProjects();
  }
  loadAllProjects() {
    this.allProjects = this.projectService.getAllProject();
  }
  onFormSubmit() {
    this.dataSaved = false;
    const project = this.projectForm.value;
    this.CreateProject(project);
    this.projectForm.reset();
  }
  loadProjectToEdit(projectId: string | undefined) {
    this.projectService.getProjectById(projectId).subscribe((project) => {
      this.massage = null;
      this.dataSaved = false;
      this.projectIdUpdate = project.id;
      this.projectForm.controls['name'].setValue(project.name);
      this.projectForm.controls['description'].setValue(project.description);
      this.projectForm.controls['deadline'].setValue(project.deadline);
      this.projectForm.controls['teamId'].setValue(project.teamId);
      this.projectForm.controls['authorId'].setValue(project.authorId);
    });
  }
  CreateProject(project: Project) {
    if (this.projectIdUpdate == undefined) {
      this.projectService.createProject(project).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record saved Successfully';
        this.loadAllProjects();
        this.projectIdUpdate = undefined;
        this.projectForm.reset();
      });
    } else {
      project.id = this.projectIdUpdate;
      this.projectService.updateProject(project).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Updated Successfully';
        this.loadAllProjects();
        this.projectIdUpdate = undefined;
        this.projectForm.reset();
      });
    }
  }
  deleteProject(projectId: string) {
    if (confirm('Are you sure you want to delete this ?')) {
      console.log('delete: ' + projectId);
      this.projectService.deleteProjectById(projectId).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Deleted Succefully';
        this.loadAllProjects();
        this.projectIdUpdate = undefined;
        this.projectForm.reset();
      });
    }
  }
  resetForm() {
    this.projectForm.reset();
    this.massage = null;
    this.dataSaved = false;
  }
}
