export class Project {
  id?: string | undefined;
  authorId?: string | undefined;
  teamId?: string | undefined;
  name?: string | undefined;
  description?: string | undefined;
  deadline?: Date | undefined;
  createdAt?: Date | undefined;
}
