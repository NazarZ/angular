import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from './project';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  url = 'https://localhost:44362';
  constructor(private http: HttpClient) {}
  getAllProject(): Observable<Project[]> {
    return this.http.get<Project[]>(this.url + '/api/Projects');
  }
  getProjectById(ProjectId: string | undefined): Observable<Project> {
    return this.http.get<Project>(this.url + '/api/Projects/' + ProjectId);
  }
  createProject(Project: Project): Observable<Project> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    Project.createdAt = new Date();
    return this.http.post<Project>(
      this.url + '/api/Projects/',
      Project,
      httpOptions
    );
  }
  updateProject(Project: Project): Observable<Project> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    console.log(Project);
    return this.http.put<Project>(
      this.url + '/api/Projects/',
      Project,
      httpOptions
    );
  }
  deleteProjectById(Projectid: string): Observable<number> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.http.delete<number>(
      this.url + '/api/Projects/' + Projectid,
      httpOptions
    );
  }
}
