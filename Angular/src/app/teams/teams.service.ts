import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from './team';

@Injectable({
  providedIn: 'root',
})
export class TeamsService {
  url = 'https://localhost:44362';
  constructor(private http: HttpClient) {}
  getAllTeam(): Observable<Team[]> {
    return this.http.get<Team[]>(this.url + '/api/Teams');
  }
  getTeamById(TeamId: string | undefined): Observable<Team> {
    return this.http.get<Team>(this.url + '/api/Teams/' + TeamId);
  }
  createTeam(Team: Team): Observable<Team> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    Team.createdAt = new Date();
    return this.http.post<Team>(this.url + '/api/Teams/', Team, httpOptions);
  }
  updateTeam(Team: Team): Observable<Team> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    console.log(Team);
    return this.http.put<Team>(this.url + '/api/Teams/', Team, httpOptions);
  }
  deleteTeamById(Teamid: string): Observable<number> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.http.delete<number>(
      this.url + '/api/Teams/' + Teamid,
      httpOptions
    );
  }
}
