import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { TeamsService } from '../teams.service';
import { Team } from '../team';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css'],
})
export class TeamsComponent implements OnInit {
  dataSaved = false;
  teamForm: any;
  allTeams?: Observable<Team[]>;
  teamIdUpdate: undefined | string = undefined;
  massage: string | null = null;
  title = 'Teams';
  constructor(
    private formbulider: FormBuilder,
    private teamService: TeamsService
  ) {}

  ngOnInit() {
    this.teamForm = this.formbulider.group({
      name: ['', [Validators.required]],
    });
    this.loadAllTeams();
  }
  loadAllTeams() {
    this.allTeams = this.teamService.getAllTeam();
  }
  onFormSubmit() {
    this.dataSaved = false;
    const team = this.teamForm.value;
    this.CreateTeam(team);
    this.teamForm.reset();
  }
  loadTeamToEdit(teamId: string | undefined) {
    this.teamService.getTeamById(teamId).subscribe((team) => {
      this.massage = null;
      this.dataSaved = false;
      this.teamIdUpdate = team.id!;
      this.teamForm.controls['name'].setValue(team.name);
    });
  }
  CreateTeam(team: Team) {
    if (this.teamIdUpdate == undefined) {
      this.teamService.createTeam(team).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record saved Successfully';
        this.loadAllTeams();
        this.teamIdUpdate = undefined;
        this.teamForm.reset();
      });
    } else {
      team.id = this.teamIdUpdate;
      this.teamService.updateTeam(team).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Updated Successfully';
        this.loadAllTeams();
        this.teamIdUpdate = undefined;
        this.teamForm.reset();
      });
    }
  }
  deleteTeam(teamId: string) {
    if (confirm('Are you sure you want to delete this ?')) {
      console.log('delete: ' + teamId);
      this.teamService.deleteTeamById(teamId).subscribe(() => {
        this.dataSaved = true;
        this.massage = 'Record Deleted Succefully';
        this.loadAllTeams();
        this.teamIdUpdate = undefined;
        this.teamForm.reset();
      });
    }
  }
  resetForm() {
    this.teamForm.reset();
    this.massage = null;
    this.dataSaved = false;
  }
}
