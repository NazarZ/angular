export class Team {
  id: string;
  name: string;
  createdAt: Date;
}
